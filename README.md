# Modul 3

## Anggota

- Irsyad Fikriansyah Ramadhan (5025211149)
- Ligar Arsa Arnata (5025211244)
- Radhiyan Muhammad Hisan (5025211166)

## Penjelasan

### soal1

1a. Untuk menyimpan frequensi tiap char, kami menggunakan struct berjudul Frequence yang memiliki attribut chaercter dan frequency. Untuk mengirim hasil penghitungan frequency dari parent process menuju child process, maka diperlukan pipe.

1b. Setelah Child mendapat data dari parent process, maka akan dilanjutkan untuk mengubahnya menjadi huffman tree.

1c. Kemudian hasil tersebut di passingkan ke parent process menggunakan pipe yang ke 2.

1d. Setelah mendapatkan hasil kompresi dari child, parent process kemudian membuat 2 file baru. File pertama berisikan bit dari file.txt tanpa menggunakan huffman dan file kedua berisikan bit dari file.txt dengan menggunakan huffman yang telah didapatkan dari child

1e. Setelah dibuat kedua file, kita tinggal menghitung jumlah char yang merepresentasikan sebagai bit (kecuali space dan newline)

![Gambar soal no 1a](Gambar/1a.jpg)
![Gambar soal no 1b](Gambar/1b.jpg)
![Gambar soal no 1c](Gambar/1c.jpg)

### soal2

2a. Pada code kalian.c, akan ada 2 key yang digunakan untuk melakukan shared memory ke 2 program yang berbeda yaitu cinta.c dan sisop.c. Selain itu, dalam code ini juga akan ada perkalian antara 2 matriks yang berukuran 4x2 dan 2x5, dimana seluruh elemen matriks didalamnya akan berisi angka random melalui perintah rand() yang telah ditetapkan batasnya. Hasil dari perkalian matriks ini akan ditampilkan di layar dan juga akan disimpan pada Shared Memory 1 dan Shared Memory 2 untuk dilanjutkan ke proses selanjutnya. Berikut merupakan salah satu contoh output hasil perkalian 2 matriks dari code kalian.c

![Gambar 2a](Gambar/2a.png)

2b. Pada code cinta.c, akan mengakses hasil perkalian dari code kalian.c yang telah disimpan dalam shared memory 1 dengan key 1234. Kemudian tiap elemen dari hasil perkalian akan dicari hasil factorial menggunak Thread. Nantinya akan ada function factorial, dimana function tersebut berguna untuk menghitung hasil factorial menggunakan Thread. Terkadang hasil dari code cinta.c ini ada yang tidak sesuai dengan hasil aslinya. Hal tersebut dikarenakan penggunaan Thread untuk menghitung factorial sangat ditentukan oleh mana proses yang selesai terlebih dahulu sehingga terdapat satu waktu dimana hasil dari factorial angkanya tidak terhitung. Berikut merupakan output dari cinta.c yang didapatkan dari shared memory 1 pada code kalian.c beserta time executionnya.

![Gambar 2b](Gambar/2b.png)
![Gambar Time Exec 2b](Gambar/time2b.png)

2c. Pada code sisop.c, akan mengakses hasil perkalian dari code kalian.c yang telah disimpan dalam shared memory 2 dengan key 2345. Yang membedakan dengan cinta.c, code ini akan menghitung hasil factorial dari tiap elemen matriks dengan menggunakan nested loop saja tanpa menggunakan Thread. Hasil akhirnya terkadang tidak bisa sama persis dengan hasil dari code cinta.c karena dengan Nested Loop seharusnya tiap elemen akan dihitung hasil factorialnya sehingga tidak menyisakan elemen yang tidak terhitung seperti pada saat menggunakan Thread. Berikut merupakan output dari sisop.c yang didapatkan dari shared memory 2 pada code kalian.c beserta time executionnya.

![Gambar 2c](Gambar/2c.png)
![Gambar Time Exec 2c](Gambar/time2c.png)

- Kesimpulannya jika hasil factorial dihitung dengan menggunakan Thread memakan waktu yang sedikit lebih lama dan hasil yang kurang akurat, sedangkan saat dihitung tanpa menggunakan Thread akan menghasilkan output dengan waktu yang sedikit lebi cepat dan perhitungan yang lebih akurat.

### soal3

3a. Pada user.c dan stream.c kami mengimplementasikan message queue pada main function masing-masing program. Saat user.c dijalankan, program akan meminta inputan yang kemudian inputan berupa string tersebut akan di kirimkan kedalam message queue dan saat stream stream.c dijalankan, program akan mengambil string yang terlah tersimpan pada message queue. message queue di set pada key 12345.

3b. Saat ./stream menerima pesan DECRYPT dari queue, maka akan dijalankan fungsi decrypt(). Isi dari fungsi tersebut adalah:

* membuat / mereset file tujuan (playlist.txt)
* meng-scan file "song-playlist.json" 
* Setiap ada kata rot13 / base64 / hex, read line berikutnya dan masukkan string kedalam fungsi decoder yang sesuai
* urutkan file hasil

3c. Saat ./stream menerima pesan LIST dari queue, maka akan dijalankan fungsi list(). Isi dari fungsi tersebut adalah:

* meng-scan file "playlist.txt" 
* print setiap line

3d. Saat ./stream menerima pesan PLAY <SONG> dari queue, maka akan dijalankan fungsi play(). Isi dari fungsi tersebut adalah:

* meng-scan file "playlist.txt" 
* menghitung berapa string yang terdapat substring <SONG> sekaligus menyimpannya kedalam sebuah char pointer
* jika hanya terdapat 1 song, maka print USER … PLAYING …
* jika terdapat n sons, maka printkan listnya
* jika tidak terdapat song, maka printkan THERE IS NO SONG CONTAINING …

3e. Saat ./stream menerima pesan ADD <SONG> dari queue, maka akan dijalankan fungsi add(). Isi dari fungsi tersebut adalah:

* meng-scan file "playlist.txt" 
* cek apakah song telah terdapat di di file
* jika tidak, append pada file kemudian urutkan
* jika iya, maka print SONG ALREADY ON PLAYLIST

3f. Untuk membatasi user yang mengirim pesan ke stream, kami menggunakan semaphore. Pada stream.c, terdapat array yang menyimpan pid yang mengirim message queue dengan jumlah 2. Jika array tersebut penuh, maka message yang akan dikirimkan setelahnya akan mengakibatkan ./stream mengeprint STREAM SYSTEM OVERLOAD

![Gambar 3f](Gambar/3f.jpg)

3g. Untuk memfilter perintah yang diberikan ./user, digunakan strncmp pada loop utama stream.c

### soal4
**a.** _Download_ dan _unzip_ menggunakan proses dan `execv` seperti shift sebelumnya<br />

**b.** Alur program:
1. Membuat folder **categorized/** menggunakan fungsi `bikin_folder()` menggunakan *command* `mkdir`
2. Membaca file **extensions.txt** menggunakan fungsi `baca_file_ext()`:<br />
&nbsp;&nbsp;a. Membersihkan hasil bacaan per baris dari karakter "\n"<br />
&nbsp;&nbsp;b. Menyimpan hasil bacaan ke array `dftr_extensions[]`<br />
&nbsp;&nbsp;c. Menambahkan isi variabel `jml_extension`<br />
3. Membaca file **max.txt** menggunakan fungsi `baca_file_max()` untuk mendapatkan isi maksimal tiap folder extension kemudian menyimpan hasilnya di variabel `jml_file_maks`
4. Membuat `thread` untuk menelusuri file-file pada folder **files/** secara rekursif menggunakan fungsi `liat_semua_file()`:<br />
&nbsp;&nbsp;a. Selama masih ada folder/file yang dibaca tetap lakukan rekursi<br />
&nbsp;&nbsp;b. Mengecek apakah yang dibaca adalah file menggunakan fungsi `apakah_file()`<br />
&nbsp;&nbsp;c. Jika ya, siapkan file untuk pemindahan menggunakan fungsi `pindahin_file()`<br />
&nbsp;&nbsp;d. Menuliskan log pengaksesan file ke file **log.txt** menggunakan fungsi `log_akses()`<br />
5. Menuliskan log pembuatan folder ke file **log.txt** menggunakan fungsi `log_bikin()`
6. menggunakan fungsi `pindahin_file()`:<br />
&nbsp;&nbsp;a. Hitung file di tiap folder menggunakan fungsi `hitung_file()` dan mengecek apakah folder penuh menggunakan fungsi `apakah_folder_penuh()`<br />
&nbsp;&nbsp;b. Membuat folder untuk tiap extension<br />
&nbsp;&nbsp;c. Membuat folder **extension(n)/** jika ada yang sudah maksimal<br />
&nbsp;&nbsp;d. Membuat folder **categorized/other/**<br />
&nbsp;&nbsp;e. Memindahkan file menggunakan `rename()`<br />
&nbsp;&nbsp;f. Menuliskan log pemindahan file ke file **log.txt** menggunakan fungsi `log_pindah()`<br />
<br />
![Gambar 4c1](Gambar/4c1.png)<br />
![Gambar 4c2](Gambar/4c2.png)<br />
**c.** Alur program:<br />
1. Membaca folder-folder dalam folder **categorized/**<br />
2. Menghitung file-file-nya menggunakan fungsi `hitung_file()`<br />
3. Mengurutkan hasil<br />
4. Menambahkan file untuk tiap folder dengan extension sama<br />
5. Menyimpan indeks untuk folder **others/**<br />
6. Menampilkan hasil tiap folder extension<br />
7. Menampilkan hasil folder **others/**<br />
<br />
**f.** Alur program:<br />
1. Membaca folder **log.txt**<br />
2. Tiap baris di cek apakah barisnya valid<br />
3. Mengecek tipe log:<br />
&nbsp;&nbsp;a. ACCESSED akan menambahkan variabel `jml_accessed`<br />
&nbsp;&nbsp;b. MADE akan menghitung folder-folder yang telah dibuat menggunakan fungsi `perbarui_folder()`<br />
&nbsp;&nbsp;c. MOVED akan menghitung file-file dalam folder-folder menggunakan fungsi `perbarui_ext()`<br />
4. Menampilkan hasil dengan cara yang mirip dengan yang ada pada file **categorize.c** (subsoal c)
