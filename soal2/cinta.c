#include <stdio.h>
#include <stdlib.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <pthread.h>

#define ROW1 4 // Row Matriks 
#define COL2 5 // Column Matriks 
#define SHM_KEY 1234 // Key Shared Memory

// Struct untuk Mengirim Argumen Thread
typedef struct {
    int row;
    int col;
    int *shm_ptr;
} thread_args;

// Fungsi untuk Menghitung Faktorial menggunakan Thread
void *factorial(void *arg)
{
    int i, n;
    long long res = 1;
    
    // Variabel Pointer to Struct
    thread_args *t_args = (thread_args *) arg;
    
    // Variabel pointer to shared memory
    int *shm_ptr = t_args->shm_ptr;
    int row = t_args->row;
    int col = t_args->col;
    
    // Mengambil nilai dari shared memory pada posisi baris dan kolom yang sesuai
    n = *(shm_ptr + row*COL2 + col); 
    
    // Menghitung faktorial dari n. Setiap iterasi akan mengalikan variabel res dengan i.
    for(i = 1; i <= n; i++) {
        res *= i;
    }
    
    //Menyimpan nilai faktorial yang dihitung di atas ke shared memory pada posisi yang sesuai.
    *(shm_ptr + row*COL2 + col) = res; 
    
    return NULL;
}

int main()
{

    // Akses Shared Memory (Mendapatkan ID nya)
    int shmid = shmget(SHM_KEY, sizeof(int[ROW1][COL2]), 0666);
    if(shmid < 0) {
        perror("shmget");
        return 1;
    }
    
    // Attach Shared Memory ke Ruang Proses
    int *shm_ptr = (int *) shmat(shmid, NULL, 0);
    if(shm_ptr == (int *) -1) {
        perror("shmat");
        return 1;
    }
    
    int i, j;
    pthread_t threads[ROW1*COL2];
    thread_args t_args[ROW1*COL2];
    
    // Menghitung Faktorial dari Elemen Matriks dengan menggunakan Fungsi Factorial
    for(i = 0; i < ROW1; i++) {
        for(j = 0; j < COL2; j++) {
            t_args[i*COL2+j].row = i;
            t_args[i*COL2+j].col = j;
            t_args[i*COL2+j].shm_ptr = shm_ptr;
            pthread_create(&threads[i*COL2+j], NULL, factorial, &t_args[i*COL2+j]);
        }
    }
    
    // Menunggu Thread Selesai
    for(i = 0; i < ROW1*COL2; i++) {
        pthread_join(threads[i], NULL);
    }
    
    // Menampilkan Hasil Faktorial
    printf("Hasil faktorial matriks:\n");
    for(i = 0; i < ROW1; i++) {
        for(j = 0; j < COL2; j++) {
            printf("%lld ", *(shm_ptr + i*COL2 + j));
        }
        printf("\n");
    }
    
    // Lepas dan Hapus Shared Memory
    shmdt(shm_ptr);
    shmctl(shmid, IPC_RMID, NULL);
    
    return 0;
}
