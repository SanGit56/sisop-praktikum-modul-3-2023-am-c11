#include <stdio.h>
#include <stdlib.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <time.h>

#define ROW1 4 // Row Matriks 1
#define COL1 2 // Column Matriks 1
#define ROW2 2 // Row Matriks 2
#define COL2 5 // Column Matriks 2
#define SHM_KEY 1234 // Key Shared Memory 1
#define SHM_KEY_2 2345 // Key Shared Memory 2

int main()
{
    int mat1[ROW1][COL1], mat2[ROW2][COL2], res[ROW1][COL2], res_2[ROW1][COL2];
    int i, j, k, sum = 0;
    
    // Inisiasi untuk Angka Random
    srand(time(0));
    
    // Pengisian Matriks 1
    for(i = 0; i < ROW1; i++) {
        for(j = 0; j < COL1; j++) {
            mat1[i][j] = rand() % 5 + 1; // Angka random 1-5 (Inklusif)
        }
    }
    
    // Pengisian Matriks 2
    for(i = 0; i < ROW2; i++) {
        for(j = 0; j < COL2; j++) {
            mat2[i][j] = rand() % 4 + 1; // Angka random 1-4 (Inklusif)
        }
    }
    
    // Perkalian Matriks 1 dan Matriks 2
    for(i = 0; i < ROW1; i++) {
        for(j = 0; j < COL2; j++) {
            sum = 0;
            for(k = 0; k < COL1; k++) {
                sum += mat1[i][k] * mat2[k][j];
            }
            res[i][j] = sum;
            res_2[i][j] = sum;
        }
    }
    
    // Menampilkan Hasil Perkalian Matriks
    printf("Hasil perkalian matriks:\n");
    for(i = 0; i < ROW1; i++) {
        for(j = 0; j < COL2; j++) {
            printf("%d ", res[i][j]);
        }
        printf("\n");
    }

    // Akses Shared Memory 1 (Mendapatkan ID nya)
    int shmid = shmget(SHM_KEY, sizeof(res), IPC_CREAT | 0666);
    if(shmid < 0) {
        perror("shmget");
        return 1;
    }
    
    // Attach Shared Memory 1 ke Ruang Proses
    int *shm_ptr = (int *) shmat(shmid, NULL, 0);
    if(shm_ptr == (int *) -1) {
        perror("shmat");
        return 1;
    }
    
    // Menyalin Hasil Perkalian Matriks ke Shared Memory 1
    for(i = 0; i < ROW1; i++) {
        for(j = 0; j < COL2; j++) {
            *(shm_ptr + i*COL2 + j) = res[i][j];
        }
    }
    
    // Akses Shared Memory 2 (Mendapatkan ID nya)
    int shmid_2 = shmget(SHM_KEY_2, sizeof(res_2), IPC_CREAT | 0666);
    if(shmid_2 < 0) {
        perror("shmget");
        return 1;
    }
    
    // Attach Shared Memory 2 ke Ruang Proses
    int *shm_ptr_2 = (int *) shmat(shmid_2, NULL, 0);
    if(shm_ptr_2 == (int *) -1) {
        perror("shmat");
        return 1;
    }
    
    // Menyalin Hasil Perkalian Matriks ke Shared Memory 2
    for(i = 0; i < ROW1; i++) {
        for(j = 0; j < COL2; j++) {
            *(shm_ptr_2 + i*COL2 + j) = res_2[i][j];
        }
    }
    
    // Lepas Shared Memory (Detach)
    shmdt(shm_ptr);
    shmdt(shm_ptr_2);

    return 0;
}
