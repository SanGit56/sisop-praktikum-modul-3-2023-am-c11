#include <stdio.h>
#include <sys/ipc.h>
#include <sys/shm.h>

#define ROW1 4 // Row Matriks 
#define COL2 5 // Column Matriks 
#define SHM_KEY_2 2345 // Key Shared Memory

int main()
{
   
    // Akses Shared Memory (Mendapatkan ID nya)
    int shmid = shmget(SHM_KEY_2, sizeof(int[ROW1][COL2]), 0666);
    if(shmid < 0) {
        perror("shmget");
        return 1;
    }

    // Attach Shared Memory ke Ruang Proses
    int *shm_ptr = (int *) shmat(shmid, NULL, 0);
    if(shm_ptr == (int *) -1) {
        perror("shmat");
        return 1;
    }
   
    // Menghitung Faktorial tiap Elemen Matriks menggunakan Nested Loop dan Menampilkannya
    printf("Hasil faktorial matriks:\n");
    for (int i = 0; i < ROW1; i++) {
        for (int j = 0; j < COL2; j++) {
            long long int fact = 1;
            for (int k = 1; k <= shm_ptr[i * COL2 + j]; k++) {
                fact *= k;
            }
            printf("%llu ", fact);
        }
        printf("\n");
    }

    // Lepas dan Hapus Shared Memory
    shmdt(shm_ptr);
    shmctl(shmid, IPC_RMID, NULL);

    return 0;
}
