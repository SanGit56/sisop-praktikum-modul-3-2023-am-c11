#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <sys/types.h>
#include <unistd.h>
#define MAX_TEXT 512  // maximum length of the message that can be sent allowed
struct my_msg {
    long int msg_type;
    char some_text[MAX_TEXT];
};

int main() {
    int running = 1;
    int msgid;
    struct my_msg data;
    char buffer[200];  // array to store user input
    msgid = msgget((key_t)12345, 0666 | IPC_CREAT);
    // -1 means the message queue is not created
    if (msgid == -1) {
        printf("Error in creating queue\n");
        exit(0);
    }

    while (running) {
        printf("Enter Command:\n");
        fgets(buffer, 200, stdin);
        data.msg_type = 1;
        sprintf(data.some_text, "%d %s", getpid(), buffer);
        if (msgsnd(msgid, (void *)&data, MAX_TEXT, 0) == -1) {
            printf("Msg not sent\n");
        }
        if (strncmp(buffer, "end", 3) == 0) {
            running = 0;
        }
        printf("\n");
    }
}
