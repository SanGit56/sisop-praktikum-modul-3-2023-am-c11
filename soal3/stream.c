#include <ctype.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <sys/types.h>
#include <unistd.h>

static char encoding_table[] =
    "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
static char *decoding_table = NULL;

void rot13(char src[]) {
    if (src == NULL) return;

    char *result = malloc(strlen(src));

    if (result != NULL) {
        strcpy(result, src);
        char *current_char = result;

        while (*current_char != '\0') {
            // Only increment alphabet characters
            if ((*current_char >= 'A' && *current_char <= 'Z') ||
                (*current_char >= 'a' && *current_char <= 'z')) {
                if (*current_char > 'm' ||
                    (*current_char > 'M' && *current_char <= 'Z')) {
                    // Characters that wrap around to the start of the alphabet
                    *current_char -= 13;
                } else {
                    // Characters that can be safely incremented
                    *current_char += 13;
                }
            }
            current_char++;
        }
    }
    strcpy(src, result);
    free(result);
}

void build_decoding_table() {
    decoding_table = malloc(256);

    for (int i = 0; i < 64; i++) {
        decoding_table[(char)encoding_table[i]] = i;
    }
}

void base64(char src[]) {
    if (decoding_table == NULL) build_decoding_table();

    long input_length = strlen(src);
    // if (input_length % 4 != 0) return;

    long output_length = input_length / 4 * 3;

    if (src[input_length - 1] == '=') (output_length)--;
    if (src[input_length - 2] == '=') (output_length)--;

    char *decoded_data = (char *)malloc(sizeof(char) * output_length);
    // if (decoded_data == NULL) return;

    for (int i = 0, j = 0; i < input_length;) {
        uint32_t sextet_a = src[i] == '=' ? 0 & i++ : decoding_table[src[i++]];
        uint32_t sextet_b = src[i] == '=' ? 0 & i++ : decoding_table[src[i++]];
        uint32_t sextet_c = src[i] == '=' ? 0 & i++ : decoding_table[src[i++]];
        uint32_t sextet_d = src[i] == '=' ? 0 & i++ : decoding_table[src[i++]];

        uint32_t triple = (sextet_a << 3 * 6) + (sextet_b << 2 * 6) +
                          (sextet_c << 1 * 6) + (sextet_d << 0 * 6);

        if (j < output_length) decoded_data[j++] = (triple >> 2 * 8) & 0xFF;
        if (j < output_length) decoded_data[j++] = (triple >> 1 * 8) & 0xFF;
        if (j < output_length) decoded_data[j++] = (triple >> 0 * 8) & 0xFF;
    }

    strcpy(src, decoded_data);
    free(decoded_data);
}

void hex(char src[]) {
    char *output = malloc(sizeof(char) * strlen(src) - 1);
    int j = 0;
    for (int i = 0; i < strlen(src) - 1; i += 2) {
        char *temp = malloc(sizeof(char) * 2);
        temp[0] = src[i];
        temp[1] = src[i + 1];
        int num = (int)strtol(temp, NULL, 16);  // number base 16
        output[j++] = (char)num;
    }
    strcpy(src, output);
    free(output);
}

char *clean_line(char src[]) {
    // ? clean line from unwanted junk
    // ? before : `: "Vzzn Or - Oynpx Rlrq Crnf"`
    // ? after  : `Vzzn Or - Oynpx Rlrq Crnf`
    char *temp = strstr(src, ": \"");
    temp += 3;
    char *end = strstr(temp, "\"");
    *end = '\0';
    return temp;
}

void decrypt() {
    char *fileName = "song-playlist.json";
    char *outputName = "playlist.txt";
    char *tempFileName = "temp.txt";
    char line[BUFSIZ], command[BUFSIZ];

    // ? reset playlist.txt if any or create if none
    sprintf(command, "> %s", outputName);
    system(command);

    FILE *file = fopen(fileName, "r");
    FILE *out = fopen(tempFileName, "a");

    // ? scan
    while (fgets(line, sizeof(line), file)) {
        if (strstr(line, "rot13")) {
            fgets(line, sizeof(line), file);
            char *temp = clean_line(line);
            rot13(temp);
            fprintf(out, "%s\n", temp);
        } else if (strstr(line, "base64")) {
            fgets(line, sizeof(line), file);
            char *temp = clean_line(line);
            base64(temp);
            fprintf(out, "%s\n", temp);
        } else if (strstr(line, "hex")) {
            fgets(line, sizeof(line), file);
            char *temp = clean_line(line);
            hex(temp);
            fprintf(out, "%s\n", temp);
        }
    }
    fclose(file);
    fclose(out);

    // ? sort
    sprintf(command, "sort %s > %s; rm %s", tempFileName, outputName,
            tempFileName);
    system(command);
}

void list() {
    char *fileName = "playlist.txt";
    FILE *file = fopen(fileName, "r");
    char line[BUFSIZ];
    while (fgets(line, sizeof(line), file)) printf("%s", line);
    fclose(file);
}

void play(char song[], int id) {
    char *fileName = "playlist.txt";
    char line[BUFSIZ];
    char list[BUFSIZ][BUFSIZ];
    int count = 0;

    // ? convert search string to lowercase
    char *lowerCaseSong = strdup(song);
    for (int i = 0; lowerCaseSong[i] != '\0'; i++) {
        lowerCaseSong[i] = tolower(lowerCaseSong[i]);
    }

    // ? scan
    FILE *file = fopen(fileName, "r");
    while (fgets(line, sizeof(line), file)) {
        char *lowerCaseline = strdup(line);
        for (int i = 0; lowerCaseline[i] != '\0'; i++) {
            lowerCaseline[i] = tolower(lowerCaseline[i]);
        }

        if (strstr(lowerCaseline, lowerCaseSong)) {
            strcpy(list[count++], line);
        }
    }

    // ? output
    if (count == 0) {
        printf("THERE IS NO SONG CONTAINING \"%s\"\n", lowerCaseSong);
    } else if (count == 1) {
        printf("USER %d PLAYING \"%s\"\n", id, song);
    } else {
        printf("THERE ARE \"%d\" SONG CONTAINING \"%s\"\n", count, song);
        for (int i = 0; i < count; i++) {
            printf("%d. %s", i + 1, list[i]);
        }
    }
    fclose(file);
}

void add(char song[], int id) {
    char *fileName = "playlist.txt";
    FILE *file = fopen(fileName, "r");
    char line[BUFSIZ];

    // ? scan
    while (fgets(line, sizeof(line), file)) {
        if (strstr(line, song)) {
            printf("SONG ALREADY ON PLAYLIST\n");
            return;
        }
    }

    // ? appending
    fclose(file);
    file = fopen(fileName, "a");
    fprintf(file, "%s\n", song);
    printf("USER %d %s\n", id, song);
    fclose(file);

    // ? sort
    char *tempFileName = "temp.txt";
    char command[BUFSIZ];
    sprintf(command, "cat %s | sort > %s", fileName, tempFileName);
    system(command);
    sprintf(command, "cat %s > %s; rm %s", tempFileName, fileName,
            tempFileName);
    system(command);
}

struct my_msg {
    long int msg_type;
    char some_text[BUFSIZ];
};

int main() {
    struct my_msg data;
    long int msg_to_rec = 0;
    int msgid, running = 1, max_user = 2;
    int *user_id = calloc(max_user, sizeof(int) * max_user);
    bool full = false;
    msgid = msgget((key_t)12345, 0666 | IPC_CREAT);

    while (running) {
        msgrcv(msgid, (void *)&data, BUFSIZ, msg_to_rec, 0);
        char *s = strdup(data.some_text);
        int id = atoi(s);

        bool duplicate = false;
        for (int i = 0; i < max_user; i++) {
            if (user_id[i] == id) {
                printf("User ID already exists.\n");
                duplicate = true;
                break;
            }
        }

        if (!duplicate) {
            bool added = false;
            for (int i = 0; i < max_user; i++) {
                if (user_id[i] == 0) {
                    user_id[i] = id;
                    added = true;
                    break;
                }
            }

            if (!added) {
                printf("User ID array is full.\n");
                full = true;
            }
        }
        printf("Current user IDs: ");
        for (int i = 0; i < max_user; i++) {
            printf("%d ", user_id[i]);
        }
        printf("\n");

        if (full) {
            printf("STREAM SYSTEM OVERLOAD\n");
            full = false;
            break;
        }
        while (*s != ' ') s++;  // removing ID
        s++;                    // removing ' '
        printf("Data received: %s", s);
        if (strncmp(s, "end", 3) == 0) {
            running = 0;
        } else if (strncmp(s, "DECRYPT", 7) == 0) {
            decrypt();
        } else if (strncmp(s, "LIST", 4) == 0) {
            list();
        } else if (strncmp(s, "PLAY", 4) == 0) {
            char *end, *temp = strdup(data.some_text);
            while (*temp != '\"') temp++;            // removing PLAY
            temp++;                                  // removing "
            if ((end = strchr(temp, '\"')) != NULL)  // removing "
                *end = '\0';
            play(temp, id);
        } else if (strncmp(s, "ADD", 3) == 0) {
            char *end, *temp = strdup(data.some_text);
            while (*temp != ' ') temp++;             // removing ADD
            temp++;                                  // removing ' '
            if ((end = strchr(temp, '\n')) != NULL)  // removing '\n'
                *end = '\0';
            add(temp, id);
        } else {
            printf("UNKNOWN COMMAND\n");
        }
        printf("\n");
    }
    msgctl(msgid, IPC_RMID, 0);

    return 0;
}