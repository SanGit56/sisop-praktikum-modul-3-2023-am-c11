// reference:
// https://www.geeksforgeeks.org/huffman-coding-greedy-algo-3/

#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <unistd.h>

#define MAX_TREE_HT 50
#define READ 0
#define WRITE 1

const char alphabet[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
const int alphabet_len = sizeof(alphabet) / sizeof(alphabet[0]);
char output[alphabet_len][20];
int output_index = 0;

typedef struct {
    char character;
    int frequency;
} Frequence;

struct MinHeapNode {
    char data;
    unsigned freq;
    struct MinHeapNode *left, *right;
};

struct MinHeap {
    unsigned size;
    unsigned capacity;
    struct MinHeapNode** array;
};

struct MinHeapNode* newNode(char data, unsigned freq) {
    struct MinHeapNode* temp =
        (struct MinHeapNode*)malloc(sizeof(struct MinHeapNode));

    temp->left = temp->right = NULL;
    temp->data = data;
    temp->freq = freq;

    return temp;
}

struct MinHeap* createMinHeap(unsigned capacity) {
    struct MinHeap* minHeap = (struct MinHeap*)malloc(sizeof(struct MinHeap));

    minHeap->size = 0;
    minHeap->capacity = capacity;
    minHeap->array = (struct MinHeapNode**)malloc(minHeap->capacity *
                                                  sizeof(struct MinHeapNode*));
    return minHeap;
}

void swapMinHeapNode(struct MinHeapNode** a, struct MinHeapNode** b) {
    struct MinHeapNode* t = *a;
    *a = *b;
    *b = t;
}

void minHeapify(struct MinHeap* minHeap, int idx) {
    int smallest = idx;
    int left = 2 * idx + 1;
    int right = 2 * idx + 2;

    if (left < minHeap->size &&
        minHeap->array[left]->freq < minHeap->array[smallest]->freq)
        smallest = left;

    if (right < minHeap->size &&
        minHeap->array[right]->freq < minHeap->array[smallest]->freq)
        smallest = right;

    if (smallest != idx) {
        swapMinHeapNode(&minHeap->array[smallest], &minHeap->array[idx]);
        minHeapify(minHeap, smallest);
    }
}

int isSizeOne(struct MinHeap* minHeap) { return (minHeap->size == 1); }

struct MinHeapNode* extractMin(struct MinHeap* minHeap) {
    struct MinHeapNode* temp = minHeap->array[0];
    minHeap->array[0] = minHeap->array[minHeap->size - 1];

    --minHeap->size;
    minHeapify(minHeap, 0);

    return temp;
}

void insertMinHeap(struct MinHeap* minHeap, struct MinHeapNode* minHeapNode) {
    ++minHeap->size;
    int i = minHeap->size - 1;

    while (i && minHeapNode->freq < minHeap->array[(i - 1) / 2]->freq) {
        minHeap->array[i] = minHeap->array[(i - 1) / 2];
        i = (i - 1) / 2;
    }

    minHeap->array[i] = minHeapNode;
}

// A standard function to build min heap
void buildMinHeap(struct MinHeap* minHeap) {
    int n = minHeap->size - 1;
    int i;

    for (i = (n - 1) / 2; i >= 0; --i) minHeapify(minHeap, i);
}

int isLeaf(struct MinHeapNode* root) { return !(root->left) && !(root->right); }

struct MinHeap* createAndBuildMinHeap(char data[], int freq[], int size) {
    struct MinHeap* minHeap = createMinHeap(size);

    for (int i = 0; i < size; ++i)
        minHeap->array[i] = newNode(data[i], freq[i]);

    minHeap->size = size;
    buildMinHeap(minHeap);

    return minHeap;
}

struct MinHeapNode* buildHuffmanTree(char data[], int freq[], int size) {
    struct MinHeapNode *left, *right, *top;

    struct MinHeap* minHeap = createAndBuildMinHeap(data, freq, size);

    while (!isSizeOne(minHeap)) {
        left = extractMin(minHeap);
        right = extractMin(minHeap);

        top = newNode('$', left->freq + right->freq);

        top->left = left;
        top->right = right;

        insertMinHeap(minHeap, top);
    }

    return extractMin(minHeap);
}

void printArr(int arr[], int n, int index) {
    for (int i = 0; i < n; i++) {
        sprintf(&output[index][i + 2], "%d", arr[i]);
    }
}

void printCodes(struct MinHeapNode* root, int arr[], int top) {
    if (root->left) {
        arr[top] = 0;
        printCodes(root->left, arr, top + 1);
    }

    if (root->right) {
        arr[top] = 1;
        printCodes(root->right, arr, top + 1);
    }

    if (isLeaf(root)) {
        output_index++;
        sprintf(output[output_index], "%c ", root->data);
        printArr(arr, top, output_index);
    }
}

void HuffmanCodes(char data[], int freq[], int size) {
    struct MinHeapNode* root = buildHuffmanTree(data, freq, size);

    int arr[MAX_TREE_HT], top = 0;
    printCodes(root, arr, top);
}

void check_frequency(int fd1) {
    Frequence frec_map[alphabet_len];
    size_t struct_size = sizeof(Frequence) * alphabet_len;

    char c;

    FILE* file;
    file = fopen("file.txt", "r");

    if (file == NULL) {
        printf("Error opening file\n");
        exit(1);
    }

    for (int i = 0; i < alphabet_len; i++) {
        frec_map[i].character = alphabet[i];
        frec_map[i].frequency = 0;
    }

    while ((c = fgetc(file)) != EOF) {
        if (isalpha(c)) {
            int index = (int)toupper(c) - 'A';
            frec_map[index].frequency++;
        }
    }

    write(fd1, frec_map, struct_size);  // write pipe
    fclose(file);
}

int main() {
    int fd1[2];
    int fd2[2];
    pid_t child_id;

    if (pipe(fd1) < 0 || pipe(fd2) < 0) {
        printf("Error creating pipes\n");
        exit(1);
    }

    child_id = fork();
    if (child_id == -1) {
        printf("Error creating process\n");
        exit(1);
    }

    // !parent process
    else if (child_id > 0) {
        close(fd1[READ]);
        check_frequency(fd1[WRITE]);
        close(fd1[WRITE]);

        char pipe_input[alphabet_len][20];
        int index = 0;

        ssize_t bytes_read = read(fd2[READ], pipe_input, sizeof(pipe_input));
        close(fd2[READ]);

        printf("len: %lu\n", sizeof(pipe_input) / sizeof(pipe_input[0]));
        for (int i = 0; i < alphabet_len; i++) {
            if (strlen(pipe_input[i]) > 0) printf("%s\n", pipe_input[i]);
        }

        FILE* file = fopen("file.txt", "r");
        // turning the original to bit
        FILE* newfile1 = fopen("newfile1.txt", "w");
        // turning the original to bit with huffman
        FILE* newfile2 = fopen("newfile2.txt", "w");

        if (!file && !newfile1 && !newfile2) {
            printf("Error opening files\n");
            exit(1);
        }

        char c;
        while ((c = fgetc(file)) != EOF) {
            if (isalpha(c)) {
                c = toupper(c);
                for (int i = 0; i < alphabet_len; i++) {
                    if (pipe_input[i][0] == c) {
                        fprintf(newfile2, "%s", pipe_input[i] + 2);
                        break;
                    }
                }
            }
            // turn the char into bits
            for (int i = 7; i >= 0; i--) {
                fprintf(newfile1, "%d", (c >> i) & 1);
                if (!isalpha(c)) {
                    fprintf(newfile2, "%d", (c >> i) & 1);
                }
            }
            fprintf(newfile1, " ");
            fprintf(newfile2, " ");
        }

        fclose(file);
        fclose(newfile1);
        fclose(newfile2);

        int bit_before = 0, bit_after = 0;
        newfile1 = fopen("newfile1.txt", "r");
        newfile2 = fopen("newfile2.txt", "r");

        if (!newfile1 && !newfile2) {
            printf("Error opening files\n");
            exit(1);
        }

        while ((c = fgetc(newfile1)) != EOF) {
            if (c != ' ' && c != '\0' && c != '\n') bit_before++;
        }
        while ((c = fgetc(newfile2)) != EOF) {
            if (c != ' ' && c != '\0' && c != '\n') bit_after++;
        }

        fclose(newfile1);
        fclose(newfile2);

        printf("total bits before huffman : %d\n", bit_before);
        printf("total bits after huffman : %d\n", bit_after);
        printf("total bit saved : %d\n", bit_before - bit_after);
    }

    // ! child process
    else {
        Frequence frec_map2[alphabet_len];
        close(fd1[WRITE]);
        read(fd1[READ], frec_map2, sizeof(Frequence) * alphabet_len);
        close(fd1[READ]);

        char chars[alphabet_len];
        int freqs[alphabet_len];
        int size = 0;
        for (int i = 0; i < alphabet_len; i++) {
            if (frec_map2[i].frequency > 0) {
                chars[size] = frec_map2[i].character;
                freqs[size] = frec_map2[i].frequency;
                size++;
            }
        }
        HuffmanCodes(chars, freqs, size);  // <- from gfg

        if (write(fd2[WRITE], output, sizeof(output)) < 0) {
            printf("Error writing to pipe2 0\n");
            exit(1);
        }

        close(fd2[WRITE]);
    }
    return 0;
}
