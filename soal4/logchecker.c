#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <libgen.h>

#define FOLDER_MAKS 512
#define EXT_MAKS 512

char* dftr_extensions[100];
int jml_extension = 0;

struct Folder {
    char name[512];
    int jml_file;
};

struct ExtCount {
    char ext[512];
    int count;
};

int jml_accessed = 0;
int jml_folder = 0;
struct Folder folders[FOLDER_MAKS];
int jml_ext = 0;
struct ExtCount hitung_ext[EXT_MAKS];

void tolowerstr(char * str){
    for(int i = 0; str[i]; i++){
        str[i] = tolower(str[i]);
    }
}

int itung_folder(const void *a, const void *b) {
    struct Folder *pa = (struct Folder*)a;
    struct Folder *pb = (struct Folder*)b;
    return pa->jml_file - pb->jml_file;
}

int itung_ext(const void *a, const void *b) {
    struct ExtCount *pa = (struct ExtCount*)a;
    struct ExtCount *pb = (struct ExtCount*)b;
    return pa->count - pb->count; 
}

void perbarui_folder(char *path, int jml_file, int isMade) {
    char *nama_folder;
    if(isMade == 0){
    	nama_folder = strdup(dirname(path));
    	strcat(nama_folder, "/");
    }
    
    for (int i = 0; i < jml_folder; i++) {
        if (strcmp(folders[i].name, nama_folder) == 0) {
            folders[i].jml_file += jml_file;
            return;
        }
    }

    for (int i = 0; i < jml_folder; i++) {
        if (strcmp(folders[i].name, path) == 0) {
            folders[i].jml_file += jml_file;
            return;
        }
    }
    
    if (isMade == 1){
        strcpy(folders[jml_folder].name, path);
        folders[jml_folder].jml_file = jml_file;
        jml_folder++;
    }
}

void perbarui_ext(char *nama_file) {
    char *ext = strrchr(nama_file, '.');
    if (ext == NULL) {
        return;
    }
    ext++;
	
    tolowerstr(ext);
    for (int i = 0; i < jml_ext; i++) {
        if (strcmp(hitung_ext[i].ext, ext) == 0) {
            hitung_ext[i].count++;
            return;
        }
    }
    
     strcpy(hitung_ext[jml_ext].ext, ext);
     hitung_ext[jml_ext].count = 1;
     jml_ext++;
}

void baca_file_ext() {
    FILE* file_ptr = fopen("extensions.txt", "r");

    if (file_ptr == NULL) {
        perror("gagal membuka file extensions.txt\n");
    }

    char buffer_ext[8];

    while (fgets(buffer_ext, sizeof(buffer_ext), file_ptr)) {
        dftr_extensions[jml_extension] = (char*) malloc(strlen(buffer_ext) + 1);
        buffer_ext[strcspn(buffer_ext, "\n") - 1] = '\0';
        strcpy(dftr_extensions[jml_extension], buffer_ext);
        jml_extension++;
    }

    fclose(file_ptr);
}

int main() {
    char baris[512];
    char tanggal[20];
    char waktu[20];
    char tipe[20];
    char rest[512];
    int jml_file;
    char nama_folder[512];
    char sumber[512];
    char tujuan[512];
    
    baca_file_ext();
    FILE *fp = fopen("log.txt", "r");

    while (fgets(baris, sizeof(baris), fp)) {
        if (sscanf(baris, "%s %s %s %s file : %s > %s", tanggal, waktu, tipe, rest, sumber, tujuan) < 4) {
            printf("gagal membaca baris log: %s", baris);
            continue;
        }

        if (strcmp(tipe, "ACCESSED") == 0) {
            jml_accessed++;
        } else if (strcmp(tipe, "MADE") == 0) {
            strcpy(nama_folder, rest);
            perbarui_folder(nama_folder, 0, 1);
        } else if (strcmp(tipe, "MOVED") == 0) {
            perbarui_folder(tujuan, 1, 0);
            perbarui_ext(sumber);
        }
    }

    fclose(fp);
    printf("Jumlah ACCESSED: %d\n", jml_accessed);

    qsort(folders, jml_folder, sizeof(struct Folder), itung_folder);
    printf("\nFolder:\n");
    for (int i = 0; i < jml_folder; i++) {
        printf("- %s : %d\n", folders[i].name, folders[i].jml_file);
    }

    qsort(hitung_ext, jml_ext, sizeof(struct ExtCount), itung_ext);
    printf("\nFile per extension:\n");
    for (int i = 0; i < jml_ext; i++) {
    	for(int j = 0; j < jml_extension; j++){
    		if (strcmp(dftr_extensions[j], hitung_ext[i].ext)==0)
                printf("- %s : %d\n", hitung_ext[i].ext, hitung_ext[i].count);
        }
    }
    printf("- %s : %d\n", basename(folders[jml_folder-1].name), folders[jml_folder-1].jml_file);

    return 0;
}