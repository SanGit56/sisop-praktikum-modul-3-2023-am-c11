#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <time.h>
#include <ctype.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <dirent.h>
#include <pthread.h>
#include <limits.h>
#include <unistd.h>
#include <errno.h>
#include <fts.h>
#include <fcntl.h>
#include <libgen.h>

typedef struct file {
    char alamat_parent[1024];
    char nama_file[128];
} file_t;

typedef struct {
    char * nama_folder;
    int count;
} folder;

pthread_mutex_t kunci_mutex;
char* dftr_extensions[100];
int jml_extension = 0;
int jml_file_maks = 0;

int cmpfolder(const void* a, const void* b){
    folder *pa = (folder*)a;
    folder *pb = (folder*)b;
    return pa->count - pb->count;
}

void log_bikin(char *nama_folder){
    char log[512];
    time_t waktu = time(NULL);
    struct tm * waktu_lokal = localtime(&waktu);
    strftime(log, sizeof(log), "%d-%m-%Y %H:%M:%S", waktu_lokal);
    
    char *folder_substr = strstr(nama_folder, "categorized");
    if (folder_substr == NULL) {
        folder_substr = nama_folder;
    }
    
    sprintf(log + strlen(log), " MADE %s\n", folder_substr);
    FILE *file_ptr = fopen("log.txt", "a");
    fputs(log, file_ptr);
    fclose(file_ptr);
}

void tolowerstr(char * str){
    for(int i = 0; str[i]; i++){
        str[i] = tolower(str[i]);
    }
}

char *ambil_ext(char *nama_file) {
    char *nama_ext = (char*)malloc(sizeof(char) * (PATH_MAX + PATH_MAX));
    char *titik = strrchr(nama_file, '.');

    if (!titik || titik == nama_file) {
        strcpy(nama_ext, "others");
        return nama_ext;
    }
    
    char kapital[100] = "";
    char hrf_kcl[100] = "";

    strcpy(kapital, (titik+1));
    strcpy(hrf_kcl, (titik+1));
    tolowerstr(hrf_kcl);

    for(int i = 0; i < jml_extension; i++){
        if(strcmp((hrf_kcl), dftr_extensions[i]) == 0){
            strcpy(nama_ext, kapital);
            return nama_ext;
        }
    }
    
    strcpy(nama_ext, "others");
    return nama_ext;
}

void log_akses(char *alamat){
    char log[512];
    time_t waktu = time(NULL);
    struct tm * waktu_lokal = localtime(&waktu);
    char str_waktu[20];
    strftime(str_waktu, 20, "%d-%m-%Y %H:%M:%S", waktu_lokal);
    
    char *alamat_substr = strstr(alamat, "files");
    if (alamat_substr == NULL) {
        alamat_substr = alamat;
    }
    
    sprintf(log, "%s ACCESSED %s\n", str_waktu, alamat_substr);
    FILE *file_ptr = fopen("log.txt", "a");
    fputs(log, file_ptr);
    fclose(file_ptr);
}

int hitung_file(char *dir){
    DIR *dir_ptr;
    struct dirent *direktori;
    int jml = 0;

    dir_ptr = opendir(dir);
    if(dir_ptr == NULL){
        return -1;
    }

    while((direktori = readdir(dir_ptr))){
        if(direktori->d_type == DT_REG){
            jml++;
        }
    }
    closedir(dir_ptr);
    return jml;
}

bool apakah_folder_penuh(char *dir, int makss) {
    int jml_file = hitung_file(dir);
    return jml_file >= makss;
}

void log_pindah(char *sumber, char *tujuan){
    char log[1024];
    memset(log, '\0', sizeof(log));
    time_t waktu = time(NULL);
    struct tm * waktu_lokal = localtime(&waktu);
    char str_waktu[20];
    strftime(str_waktu, 20, "%d-%m-%Y %H:%M:%S", waktu_lokal);
    
    char *sumber_substr = strstr(sumber, "files");
    if (sumber_substr == NULL) {
        sumber_substr = sumber;
    }

    char *tujuan_substr = strstr(tujuan, "categorized");
    if (tujuan_substr == NULL) {
        tujuan_substr = tujuan;
    }
    
    for (int i = 0; sumber_substr[i] != '\0'; i++) {
      if (sumber_substr[i] == ' ') {
         sumber_substr[i] = '_';
      }
    }
    
    for (int i = 0; tujuan_substr[i] != '\0'; i++) {
      if (tujuan_substr[i] == ' ') {
         tujuan_substr[i] = '_';
      }
    }
    
    sprintf(log, "%s MOVED %s file : %s > %s\n", str_waktu, ambil_ext(sumber), sumber_substr, tujuan_substr);
    FILE *file_ptr = fopen("log.txt", "a");
    fputs(log, file_ptr);
    fclose(file_ptr);
}

void bikin_folder(char nama_folder[]) {
    pid_t child_id;
    child_id = fork();

    if (child_id < 0) {
        printf("fork gagal!\n");
        exit(1);
    }

    if (child_id == 0) {
        char *argv[] = {"mkdir", "-p", nama_folder, NULL};
        execv("/bin/mkdir", argv);
    }

    int status;
    wait(&status);
}

void* pindahin_file(void* args){
    file_t *struct_file = (file_t*)args;
    char *jenis_ext = ambil_ext(struct_file->nama_file);
    char * alamat = (char*)malloc(sizeof(char) * (PATH_MAX + PATH_MAX));

    tolowerstr(jenis_ext);
    sprintf(alamat, "categorized/%s", jenis_ext);

    int jml_file = hitung_file(alamat);
    int apa_sdh_ada = 0;

    if (strcmp(jenis_ext, "others") != 0 && jml_file >= jml_file_maks) {
        char folder_baru[PATH_MAX + PATH_MAX];

        for (int i = 2; ; i++) {
            sprintf(folder_baru, "%s(%d)", alamat, i);

            if (access(folder_baru, F_OK) == -1) {
                bikin_folder(folder_baru);
                apa_sdh_ada = 1;
                break;
            } else if (!apakah_folder_penuh(folder_baru, jml_file_maks)) {
                break;
            }
        }

        strcpy(alamat, folder_baru);
    }

    bikin_folder(alamat);
    log_bikin(alamat);

    pthread_mutex_lock(&kunci_mutex);

    strcat(alamat, "/");
    char buffer[PATH_MAX + PATH_MAX];
    strcpy(buffer, alamat);
    strcat(buffer, basename(struct_file->nama_file));

    rename(struct_file->nama_file, buffer);

    pthread_mutex_unlock(&kunci_mutex);
    log_pindah(struct_file->nama_file, buffer);

    free(jenis_ext);
    free(struct_file);
}

int apakah_file(const char* path) {
    struct stat path_stat;

    if (stat(path, &path_stat) != 0) {
        perror("stat");
        return -1;
    }

    if (S_ISREG(path_stat.st_mode)) {
        return 1;
    } else if (S_ISDIR(path_stat.st_mode)) {
        return 0;
    } else {
        return -1;
    }
}

void liat_semua_file(char *dir_skrg, pthread_t *thread) {
    struct dirent *dir_ptr;
    DIR *dir = opendir(dir_skrg);

    if (!dir)
        return;

    while ((dir_ptr = readdir(dir)) != NULL) {
        if (strcmp(dir_ptr->d_name, ".") != 0 && strcmp(dir_ptr->d_name, "..") != 0) {
            pthread_t *thread_rekursi = (pthread_t*)malloc(sizeof(pthread_t));
            char alamat[512];
            
            strcpy(alamat, dir_skrg);
            strcat(alamat, "/");
            strcat(alamat, dir_ptr->d_name);

            if (apakah_file(alamat)) {
                file_t *struct_file = (file_t*)malloc(sizeof(file_t));
                
                strcpy(struct_file->alamat_parent, dir_skrg);
                strcpy(struct_file->nama_file, alamat);
                
                pthread_create(thread_rekursi, NULL, pindahin_file, (void*)struct_file);
                pthread_join(*thread_rekursi, NULL);
                free(thread_rekursi);
                continue;
            } else {
            	log_akses(alamat);
            }
            
            pthread_create(thread_rekursi, NULL, (void *)liat_semua_file,  alamat);
            pthread_join(*thread_rekursi, NULL);
            free(thread_rekursi);
        }
    }

    closedir(dir);
}

int baca_file_max() {
    FILE *file_ptr = fopen("max.txt", "r");
    int angka;
    fscanf(file_ptr, "%d", &angka);
    fclose(file_ptr);
    return angka;
}

void baca_file_ext() {
    FILE* file_ptr = fopen("extensions.txt", "r");

    if (file_ptr == NULL) {
        perror("gagal membuka file extensions.txt\n");
    }

    char buffer_ext[8];

    while (fgets(buffer_ext, sizeof(buffer_ext), file_ptr)) {
        dftr_extensions[jml_extension] = (char*) malloc(strlen(buffer_ext) + 1);
        buffer_ext[strcspn(buffer_ext, "\n") - 1] = '\0';
        strcpy(dftr_extensions[jml_extension], buffer_ext);
        jml_extension++;
    }

    fclose(file_ptr);
}

int main(){
    bikin_folder("categorized");
    baca_file_ext();
    jml_file_maks = baca_file_max();

    pthread_t *thread_liat_file = (pthread_t*)malloc(sizeof(pthread_t));
    pthread_create(thread_liat_file, NULL, (void *)liat_semua_file, "files");
    pthread_join(*thread_liat_file,NULL);
    free(thread_liat_file);

    char * alamat_parent = (char*)malloc(sizeof(char)*1024);
    strcpy(alamat_parent,"categorized");
    log_akses(alamat_parent);
    DIR *dir_ptr;
    struct dirent *direktori;
    dir_ptr = opendir(alamat_parent);
    
    for(int i = 0; i < jml_extension; i++){
        char * nama_folder = (char*)malloc(sizeof(char)*PATH_MAX);
        sprintf(nama_folder, "%s/%s", alamat_parent, dftr_extensions[i]);
        bikin_folder(nama_folder);
        log_bikin(nama_folder);
        free(nama_folder);
    }

    folder * dftr_folder = (folder*)malloc(sizeof(folder)*100);
    int jml_folder = 0;
    
    if (dir_ptr != NULL)
    {
        while (direktori = readdir(dir_ptr)){
            if(strcmp(direktori->d_name, ".") == 0 || strcmp(direktori->d_name, "..") == 0) continue;
            if(direktori->d_type == DT_DIR){
                pthread_t *thread = (pthread_t*)malloc(sizeof(pthread_t));
                char * curDirCopy = (char*)malloc(sizeof(char)*PATH_MAX);
                sprintf(curDirCopy, "%s/%s", alamat_parent, direktori->d_name);
                dftr_folder[jml_folder].nama_folder = curDirCopy;
                dftr_folder[jml_folder++].count = hitung_file(curDirCopy);
            }
        }

        closedir (dir_ptr);
    }

    qsort(dftr_folder, jml_folder, sizeof(folder), cmpfolder);
    pthread_mutex_lock(&kunci_mutex);
    
    int hitung_ext = 0;
    folder * dftr_ext = (folder*)malloc(sizeof(folder)*100);

    for(int i = 0; i < jml_folder; i++){
        char * nama_folder = strtok(basename(dftr_folder[i].nama_folder), "(");
        int j;

        for(j = 0; j < hitung_ext; j++){
            if(strcmp(nama_folder, dftr_ext[j].nama_folder) == 0){
                dftr_ext[j].count += dftr_folder[i].count;
                break;
            }
        }

        if(j < hitung_ext) continue;

        dftr_ext[hitung_ext].nama_folder = nama_folder;
        dftr_ext[hitung_ext].count = dftr_folder[i].count;
        hitung_ext++;
    }

    qsort(dftr_ext, hitung_ext, sizeof(folder), cmpfolder);
    int idxOther = 0;

    for(int i = 0; i < hitung_ext; i++){
    	if(strcmp(dftr_ext[i].nama_folder, "others") == 0){
            idxOther = i;
            continue;
        }
        printf("%s : %d\n", dftr_ext[i].nama_folder, dftr_ext[i].count);
    }

    printf("%s : %d\n", dftr_ext[idxOther].nama_folder, dftr_ext[idxOther].count);

    pthread_mutex_unlock(&kunci_mutex);
    free(dftr_folder);
    free(alamat_parent);

    return 0;
}