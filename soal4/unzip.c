#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <time.h>
#include <ctype.h>
#include <dirent.h>
#include <sys/wait.h>
#include <unistd.h>

void ekstrak() {
    pid_t child_id;
    child_id = fork();
    int status;

    if (child_id < 0) {
        printf("fork failed!\n");
        exit(1);
    }

    if (child_id == 0) {
        char *argv[] = {"unzip", "-q", "hehe.zip", NULL};
        execv("/bin/unzip", argv);
    }
    
    wait(&status);
}

void unduh() {
    pid_t child_id;
    child_id = fork();
    int status;

    if (child_id < 0) {
        printf("fork gagal!\n");
        exit(1);
    }

    if (child_id == 0) {
        char *argv[] = {"wget", "-qO", "hehe.zip", "https://drive.google.com/uc?id=1rsR6jTBss1dJh2jdEKKeUyTtTRi_0fqp&export=download", NULL};
        execv("/bin/wget", argv);
    }
    
    wait(&status);
}

int main() {
    // membuat proses
    pid_t pid, sid;
    pid = fork();

    if (pid > 0) {
        exit(EXIT_SUCCESS);
    }

    if (pid < 0) {
        exit(EXIT_FAILURE);
    }

    unduh();
    ekstrak();
    
    return 0;
}